import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import { Button, Input, Typography } from "@material-ui/core";

const backendClient = new BackendClient();

const MAX_PAGE = 10;

export const DashboardPage: FC<RouteComponentProps> = () => {
    const [users, setUsers] = useState<IUserProps[]>([]);
    const [loading, setLoading] = useState<boolean>(true)
    // const loading = true;
    const [page, setPage] = useState<number>(0);
    const [pageUsers, setPageUsers] = useState<IUserProps[]>([])
    const [filtro, setFiltro] = useState<any>('')

    useEffect(() => {
        const fetchData = async () => {
            const result = await backendClient.getAllUsers();
            console.log(result.data)
            setUsers(result.data);
            setLoading(false);
            setPageUsers(result.data.slice(0, MAX_PAGE))
        };

        fetchData();
    }, []);

    const prevPage = (e: any) => {
        setPage(page - 1)
    }

    const nextPage = (e: any) => {
        setPage(page + 1)
    }

    useEffect(() => {
        setPageUsers(users.slice(page * MAX_PAGE, page * MAX_PAGE + MAX_PAGE))
    }, [page])

    const filtrar = (e: any) => {
        setFiltro(e.target.value)
    }

    return (
        <div style={{ paddingTop: "30px" }}>

            <div>
                <Input placeholder="filtro" onChange={filtrar} />
            </div>

            <div
                style={{ display: "flex", justifyContent: "space-between" }}>
                <Button onClick={prevPage} disabled={page <= 0}>-1</Button>
                <Typography>{page}</Typography>
                <Button onClick={nextPage}>+1</Button>
            </div>

            <div style={{ display: "flex", justifyContent: "space-between", flexDirection: "column" }}>
                {loading ? (
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            height: "100vh",
                        }}
                    >
                        <CircularProgress size="60px" />
                    </div>
                ) : (
                    <div>
                        {pageUsers.length
                            ? pageUsers.map((user) => {
                                return <UserCard key={user.id} {...user} />;
                            })
                            : null}
                    </div>
                )}
            </div>
        </div >
    );
};
